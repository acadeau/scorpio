from individu import (get_gene, create_individu)
import metier
import math
import data_manager
import random
from const import (TAUX_VARIATION_HAUTEUR_COUPE, HAUTEUR_COUPE, OBJECTIF)

# Fonction de fitness permetant d'attribuer un score a chaque individu
# On utilise les Exception pour attribuer des scores faibles lorsque l'individu 
# provoque des erreurs (ne tire pas, ...)
def fitness(individu):
  try:
    K = metier.ressort(young = get_gene("young", individu), poisson = get_gene("poisson", individu))
    Lv = metier.longueur_a_vide(longueur_bras = get_gene("longueur_bras", individu), longueur_corde = get_gene("longueur_corde", individu))
    Ld = metier.longueur_deplacement(longueur_fleche = get_gene("longueur_fleche", individu), longueur_vide = Lv)
    mp = metier.masse_projectile(masse_volumique = get_gene("masse_volumique_fleche", individu), diametre_fleche = get_gene("diametre_fleche", individu),longueur_fleche = get_gene("longueur_fleche", individu))
    V = metier.velocite(ressort = K, longueur_deplacement =Ld, masse_projectile = mp)
    d = metier.portee(velocite = V, gravite = metier.GRAVITE, angle = math.radians(get_gene("angle", individu)))
    Ec = metier.energie_impact_joule(masse_projectile = mp, velocite = V)
    Etnt = metier.energie_impact_tnt(energie_impact_joule = Ec)
    metier.limite(get_gene("longueur_fleche", individu), get_gene("longueur_corde", individu), get_gene("longueur_bras", individu), get_gene("base_bras", individu), get_gene("hauteur_bras", individu), K, get_gene("young", individu), Lv, Ld)
    # Permet juste de faire les courbes
    data_manager.register("portee", d)  
    # Permet juste de faire les courbes
    data_manager.register("etnt", Etnt)
    score = (0.9*(1 / (math.fabs(d - OBJECTIF) + 1 ))) * (Etnt*Etnt)
  except (ZeroDivisionError, ValueError, Exception) as e:
    score = 0.000000001
  return (individu, score)

# Retourne une population de n individu
def generate_pop(n) :
  return list(map(create_individu, range(n)))


# Retourne un couple d'enfants avec le croisement des gênes
# La hauteur de la coupe est selectionee selon la fonction get_hauteur_coupe
def cross(couple) :
  hauteur_coupe = get_hauteur_coupe()
  genesA = couple[0].split(";")
  genesB = couple[1].split(";")
  coupe = math.floor(len(genesA)*(hauteur_coupe / 100))
  childA_geneB = genesB[- (len(genesB) - coupe) : len(genesB)] if len(genesB) - coupe != 0 else [] 
  childB_geneA = genesA[- (len(genesA) - coupe) : len(genesA)] if len(genesA) - coupe != 0 else [] 
  childA = ";".join(genesA[0 : coupe] + childA_geneB)
  childB = ";".join(genesB[0 : coupe] + childB_geneA)
  return [childA, childB]

# Retourne une hauteur de coupe.
# Elle change en fonction d'une valeur aleatoire genere pour chaque croisement
# Si cette valeur aleatoire est en dessous de la constante "TAUX_VARIATION_HAUTEUR_COUPE"
# Alors on va generer aleatoire la hauteur. Sinon c'est la valeur de HAUTEUR_COUPE qui sera donne
def get_hauteur_coupe():
  will_change = random.randint(0,100)
  if will_change < TAUX_VARIATION_HAUTEUR_COUPE:
    hauteur = random.randint(0,100)
  else:
    hauteur = HAUTEUR_COUPE
  return hauteur

# Retourne le meilleur individu selectionne par tournois
def tournament(pop, k):
  best = None
  best_score = None
  for i in range(1,k):
      (ind, score) = random.choice(pop)
      if (best == None) or score > best_score:
          best = ind
          best_score = score
  return best

# Exerce la selection des individus. 
# Il retournera un tableau qui aura taille_population / 2.
# Ce tableau contiendra les individus chanceux pouvent assurer une descendance
def selection(pop, nb_pop):
  parents = []
  while len(parents)!=nb_pop/2:
    couple = [tournament(pop, 15)]
    while len(couple) != 2:
      parent = tournament(pop, 15)
      if couple[0] != parent :
        couple.append(parent)
    parents.append(couple)
  return parents