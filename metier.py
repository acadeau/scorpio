import math
import random

# Gravite terrestre, on suppose qu'on utilise nos scorpions sur Terre.
GRAVITE = 9.81

# Liste des modules de young des materiaux existant
MATERIAUX_YOUNG = [210, 62, 350, 78, 287, random.randint(20,40), random.randint(11, 13), 20, 16, 110, random.uniform(2.2, 2.7), 450, random.randint(450, 650), 150, 128, 1220, 195, 208, random.randint(80, 130), random.randint(37, 75), 145, 34.5, random.randint(80,100), random.randint(12, 35), 44, random.randint(118, 130), 200, random.randint(61,90), 329, 2.5, 207, 2.9, 78, 550, random.randint(3,5), 0.15, 170, 3.1, 15, 1.3, random.uniform(2.8, 3.4), random.uniform(0.35, 2.5), 420, 0.8, 0.5, 114, random.randint(360, 410), random.randint(50, 100), 90]

# Liste des coefficents de poisson des materiaux existant
MATERIAUX_POISSON=[random.uniform(0.24, 0.30),random.uniform(0.24, 0.33), 0.03, 0.20, 0.17, 0.33, 0.3, random.uniform(0.21, 0.29), random.uniform(0.21, 0.26), 0.37, 0.0, 0.35, 0.35, 0.31, 0.42, random.uniform(0.40, 0.43), 0.44, 0.34, random.uniform(0.18, 0.30)]

# Liste des masses volumiques des materiaux existant
MATERIAUX_MASSE_VOLUMIQUE= [7850, 2700, 3950, 10500, 1848, random.randint(2200, 2500), random.randint(600, 1000), random.randint(8730, 8750), random.randint(920, 990), 3210, 15630, 8910, 8920, 3517, 8100, 7860, random.randint(7100, 7300), random.randint(1800, 2500), 8130, 22560, 1440, 8470, 240, 1740, 8720, 7200, random.randint(2700, 2800), 10200, 1250, 8900, random.randint(1120, 1160), 18900, 22610, random.randint(700, 800), random.randint(870, 910), 930, 21450, 1180, 11300, 910, 1050, 1350, 3950, 1250, 2200, 4500, 19350, 2320, 7140]

#sur la selection des parents --> on peut avoir deux fois le même couple``
# hauteur de coupe --> fav les pourcentages
# taux de variation de hauteur --> entre 50 et 70% --> Y a 50% de chacne que ta hauteru de coupe evolue 

#
def ressort(young, poisson):
  return (1/3)*(young/(1-2*poisson))

def longueur_a_vide(longueur_bras, longueur_corde):
  return (1/2)*math.sqrt(longueur_bras**2 - longueur_corde**2)

def longueur_deplacement(longueur_fleche, longueur_vide):
  return longueur_fleche - longueur_vide

def masse_projectile(masse_volumique, diametre_fleche, longueur_fleche):
  return  masse_volumique*(math.pi * (diametre_fleche*diametre_fleche/4))*longueur_fleche

def velocite(ressort, longueur_deplacement, masse_projectile):
  return math.sqrt((ressort*longueur_deplacement*longueur_deplacement)/masse_projectile)

def portee(velocite, gravite, angle):
  return (velocite*velocite/gravite)*math.sin(2*angle)

def energie_impact_joule(masse_projectile, velocite):
  return (1/2)*masse_projectile*velocite*velocite

def energie_impact_tnt(energie_impact_joule):
  return energie_impact_joule/4184

def moment_quadratique(section_bras_base, section_bras_hauteur):
  return (section_bras_base*section_bras_base * section_bras_hauteur)/12

def force_traction(ressort, longueur_fleche):
  return ressort*longueur_fleche

def fleche_bras(force_traction, longueur_bras, young, moment):
  return (force_traction*math.pow(longueur_bras, 3))/(48*young*moment)

# Gere les cas limites de nos scorpions. Si la fonction renvoit une 
# Exception, l'individu sera considere comme mauvais
def limite(longueur_fleche, longueur_corde, longueur_bras, base_bras, hauteur_bras, K, E, lv, ld):
  if lv > longueur_fleche:
    raise Exception("longueur vide > longueur fleche")
  elif longueur_corde > longueur_bras:
    raise Exception("longueur corde > longueur bras")
  else:
    I = moment_quadratique(section_bras_base = base_bras, section_bras_hauteur = hauteur_bras)
    F = force_traction(ressort = K, longueur_fleche = longueur_fleche)
    f = fleche_bras(force_traction = F, longueur_bras = longueur_bras, young = E, moment = I)
    if ld > f:
      raise Exception("longueur distance > longueur fleche bras")
    else: 
      return True
