import math
import random
import numpy 
import matplotlib.pyplot as plt
from functools import reduce
import collections

GRAVITE = 9.81
HAUTEUR_COUPE = 50
TAUX_VARIATION_HAUTEUR_COUPE = 60
TAUX_MUTATION = 1

NB_POPULATION = 500
NB_GENERATION = 500

MATERIAUX_YOUNG = [210, 62, 350, 78, 287, random.randint(20,40), random.randint(11, 13), 20, 16, 110, random.uniform(2.2, 2.7), 450, random.randint(450, 650), 150, 128, 1220, 195, 208, random.randint(80, 130), random.randint(37, 75), 145, 34.5, random.randint(80,100), random.randint(12, 35), 44, random.randint(118, 130), 200, random.randint(61,90), 329, 2.5, 207, 2.9, 78, 550, random.randint(3,5), 0.15, 170, 3.1, 15, 1.3, random.uniform(2.8, 3.4), random.uniform(0.35, 2.5), 420, 0.8, 0.5, 114, random.randint(360, 410), random.randint(50, 100), 90]

MATERIAUX_POISSON=[random.uniform(0.24, 0.30),random.uniform(0.24, 0.33), 0.03, 0.20, 0.17, 0.33, 0.3, random.uniform(0.21, 0.29), random.uniform(0.21, 0.26), 0.37, 0.0, 0.35, 0.35, 0.31, 0.42, random.uniform(0.40, 0.43), 0.44, 0.34, random.uniform(0.18, 0.30)]

MATERIAUX_MASSE_VOLUMIQUE= [7850, 2700, 3950, 10500, 1848, random.randint(2200, 2500), random.randint(600, 1000), random.randint(8730, 8750), random.randint(920, 990), 3210, 15630, 8910, 8920, 3517, 8100, 7860, random.randint(7100, 7300), random.randint(1800, 2500), 8130, 22560, 1440, 8470, 240, 1740, 8720, 7200, random.randint(2700, 2800), 10200, 1250, 8900, random.randint(1120, 1160), 18900, 22610, random.randint(700, 800), random.randint(870, 910), 930, 21450, 1180, 11300, 910, 1050, 1350, 3950, 1250, 2200, 4500, 19350, 2320, 7140]

#sur la selection des parents --> on peut avoir deux fois le même couple``
# hauteur de coupe --> fav les pourcentages
# taux de variation de hauteur --> entre 50 et 70% --> Y a 50% de chacne que ta hauteru de coupe evolue 

OBJECTIF = 350

def ressort(young, poisson):
  return (1/3)*(young/(1-2*poisson))

def longueur_a_vide(longueur_bras, longueur_corde):
  return (1/2)*math.sqrt(longueur_bras * longueur_bras - longueur_corde * longueur_corde)

def longueur_deplacement(longueur_fleche, longueur_vide):
  return longueur_fleche - longueur_vide

def masse_projectile(masse_volumique, diametre_fleche, longueur_fleche):
  return  masse_volumique*(math.pi * (diametre_fleche*diametre_fleche/4))*longueur_fleche

def velocite(ressort, longueur_deplacement, masse_projectile):
  return math.sqrt((ressort*longueur_deplacement*longueur_deplacement)/masse_projectile)

def portee(velocite, gravite, angle):
  return (velocite*velocite/gravite)*math.sin(2*angle)

def energie_impact_joule(masse_projectile, velocite):
  print(masse_projectile, velocite)
  return (1/2)*masse_projectile*velocite*velocite

def energie_impact_tnt(energie_impact_joule):
  return energie_impact_joule/4184

def moment_quadratique(section_bras_base, section_bras_hauteur):
  return (section_bras_base * section_bras_hauteur * section_bras_hauteur)/12

def force_traction(ressort, longueur_fleche):
  return ressort*longueur_fleche

def fleche_bras(force_traction, longueur_bras, young, moment):
  return (force_traction*math.pow(longueur_bras, 3))/(48*young*moment)

def limite(individu, K, E, lv, ld):
  if lv > individu["long_fleche"]:
    raise Exception("longueur vide > longueur fleche")
  elif individu["long_corde"] > individu["long_b"]:
    raise Exception("longueur corde > longueur bras")
  else:
    I = moment_quadratique(section_bras_base = individu["b"], section_bras_hauteur = individu["h"])
    F = force_traction(ressort = K, longueur_fleche = individu["long_fleche"])
    f = fleche_bras(force_traction = F, longueur_bras = individu["long_b"], young = E, moment = I)
    if ld > f:
      raise Exception("longueur distance > longueur fleche bras")
    else: 
      return True

portees = []
Etnt_arr = []

def fitness(individu):
  score = 0
  try:
    K = ressort(young =individu["young"], poisson = individu["poisson"])
    Lv = longueur_a_vide(longueur_bras = individu["long_b"], longueur_corde = individu["long_corde"])
    Ld = longueur_deplacement(longueur_fleche = individu["long_fleche"], longueur_vide = Lv)
    mp = masse_projectile(masse_volumique = individu["masse_volumique_fleche"], diametre_fleche = individu["diam_fleche"],longueur_fleche = individu["long_fleche"])
    V = velocite(ressort = K, longueur_deplacement = Ld, masse_projectile = mp)
    d = portee(velocite = V, gravite = GRAVITE, angle = math.radians(individu["angle"]))
    Ec = energie_impact_joule(masse_projectile = mp, velocite = V)
    Etnt = energie_impact_tnt(energie_impact_joule = Ec)
    limite(individu, K, individu["young"], Lv, Ld)
    portees.append(d)
    Etnt_arr.append(Etnt)
    score = 0.3 * (1 / (math.fabs(d - OBJECTIF) + 1 )) + 0.7 * (1/1+Etnt)
  except (ZeroDivisionError, ValueError, Exception) as e:
    score = 0.000000001
  return { 
      "individu": individu,
      "score": score
  }
  

def generate_pop(n) :
  a = []
  for i in range(0,n):
    a.append({
      "angle": random.randint(0,90),
      "long_b": random.randint(1,50),
      "b": random.uniform(0.05,0.80),
      "h": random.uniform(0.05,0.80),
      "diam_fleche": random.uniform(0.05,3),
      "long_corde": random.randint(1,50),
      "long_fleche": random.randint(1,50),
      "poisson": random.choice(MATERIAUX_POISSON),
      "young": random.choice(MATERIAUX_YOUNG),
      #"masse_volumique_fleche": random.randint(600, 1000) # On prend du bois pour être sûr d'envoyer la fleche aussi loin
      "masse_volumique_fleche": random.choice(MATERIAUX_MASSE_VOLUMIQUE) # On prend du bois pour être sûr d'envoyer la fleche aussi loin
    })
  return a

def sum(arr, individu) :
  ind = {
    "individu": individu["individu"],
    "score": individu["score"],
    "cumulative_score": arr[-1]["cumulative_score"] + individu["score"]
  }
  arr.append(ind)
  return arr

def flatten(d, parent_key="", sep="_"):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def tournament(pop, k):
  best = None
  for i in range(1,k):
      ind = pop[random.randint(0, len(pop)-1)]
      if (best == None) or ind["score"] > best["score"]:
          best = ind
  return best




def selection(cum_scores, offset):
  parents = []
  #pos = offset
  while len(parents)!=NB_POPULATION/2:
    couple = []
    while len(couple) != 2:
      parent = tournament(cum_scores, 15)
      #parent = next((indiv for indiv in cum_scores if pos <= indiv['cumulative_score']),None)
      if len(couple) == 1 :
        if (len(set(flatten(couple[0]).items()) ^ set(flatten(parent["individu"]).items())) > 0) :
          couple.append(parent["individu"])
      else :
        couple.append(parent["individu"])
      #pos =(pos + offset)%cum_scores[-1]["cumulative_score"]
    parents.append(couple)
  return parents

def get_hauteur_coupe():
  will_change = random.randint(0,100)
  if will_change < TAUX_VARIATION_HAUTEUR_COUPE:
    hauteur = random.randint(0,100)
  else:
    hauteur = HAUTEUR_COUPE
  return hauteur


def cross(couple) :
  hauteur_coupe = get_hauteur_coupe()
  coupe = math.floor(len(couple[0])*(hauteur_coupe / 100))
  keys = list(couple[0].keys())
  keys_change = keys[ - coupe : len(keys)]
  genesA = { k:v for k,v in couple[0].items() if not(k in keys_change) }
  genesB = { k:v for k,v in couple[1].items() if k in keys_change }
  childA = dict(genesA, **genesB)
  genesBA = { k:v for k,v in couple[0].items() if k in keys_change }
  genesBB = { k:v for k,v in couple[1].items() if not(k in keys_change) }
  childB = dict(genesBA, **genesBB)
  return [childA, childB]

def mutate(child) :
  if random.uniform(0,100) < TAUX_MUTATION:
    child_keys = list(child.keys())
    mutation_key = child_keys[random.randint(0, len(child_keys) - 1)]
    if mutation_key == "angle":
      new_value = {mutation_key : random.randint(0, 90)}
    elif mutation_key == "long_b":
      new_value = {mutation_key : random.randint(1,50)}
    elif mutation_key == "b":
      new_value = {mutation_key : random.uniform(0.05,0.80)}
    elif mutation_key == "h":
      new_value = {mutation_key : random.uniform(0.05,0.80)}
    elif mutation_key == "diam_fleche":
      new_value = {mutation_key : random.uniform(0.05,3)}
    elif mutation_key == "long_corde":
      new_value = {mutation_key : random.randint(1,50)}
    elif mutation_key == "long_fleche":
      new_value = {mutation_key : random.randint(1,50)}
    elif mutation_key == "poisson":
      new_value = {mutation_key : random.choice(MATERIAUX_POISSON)}
    elif mutation_key == "young":
      new_value = {mutation_key : random.choice(MATERIAUX_YOUNG)}
    elif mutation_key == "masse_volumique_fleche":
      #new_value = {mutation_key : random.randint(600, 1000)}
      new_value = {mutation_key : random.choice(MATERIAUX_MASSE_VOLUMIQUE)}
    else :
      new_value = {mutation_key : random.randint(1, 100000)}
    child.update(new_value)
  return child
    

population = generate_pop(NB_POPULATION)

variances = []
moyenne = []
moy_portee = []
moy_etnt = []

def flat_map(func, list_to_change):
  new_list = list(map(func, list_to_change))
  return [item for sublist in new_list for item in sublist]


for i in range(0, NB_GENERATION):
  pop_scores = list(map(fitness, population))
  cum_scores = reduce(sum, pop_scores, [{
    "individu": pop_scores[0]["individu"],
    "score": pop_scores[0]["score"],
    "cumulative_score": pop_scores[0]["score"]
  }])
  moy = numpy.mean(list(map(lambda x: x["score"], cum_scores)))
  variance = numpy.var(list(map(lambda x: x["score"], cum_scores)))
  variances.append(variance)
  moyenne.append(moy)
  moy_portee.append(numpy.mean(portees))
  moy_etnt.append(numpy.mean(Etnt_arr))
  Etnt_arr = []
  portees = []
  offset = random.random()%cum_scores[-1]["cumulative_score"]
  couples = selection(cum_scores, offset)
  population = list(map(mutate, flat_map(cross, couples)))
  print(i)

plt.subplot(221)
plt.plot(variances, "-", color="blue")
plt.subplot(222)
plt.plot(moyenne, "-", color="yellow")
plt.subplot(223)
plt.plot(moy_portee, "r-", OBJECTIF, "b-")
plt.subplot(224)
plt.plot(moy_etnt, "-", color="black")

plt.show()