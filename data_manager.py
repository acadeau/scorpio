import population
import numpy
import matplotlib.pyplot as plt

data = {
  "portee": [],
  "etnt": [],
  "moy_score": [],
  "variance_score": [],
  "moy_etnt": [],
  "moy_portee": [],
  "max_score": [],
  "min_score": []
}

# Reset le tableau d'un nom donne
def reset_data(name):
  data[name] = []

# Recupere le tableau en fonction du nom donne
def get_data(name):
  return data[name]

# Enregistre une nouvelle valeur dans le tableau (on utilise le nom pour le trouver)
def register(name, value):
  data[name].append(value)
  return data[name]

# Calcule les moyennes des scores et la variance
def register_score(pop):
  scores = list(map(lambda ind_score: ind_score[1], pop))
  moy = numpy.mean(scores)
  var = numpy.var(scores)
  min_score = numpy.min(scores)
  max_score = numpy.max(scores)
  register("moy_score", moy)
  register("variance_score", var)
  register("min_score", min_score)
  register("max_score", max_score)
  return (moy, var)

# Calcule les moyennes de portee et d'Etnt d'une generation
def calculate_moy_genes():
  moy_portee = numpy.mean(get_data("portee"))
  moy_etnt = numpy.mean(get_data("etnt"))
  register("moy_etnt", moy_etnt)
  register("moy_portee", moy_portee)
  reset_data("etnt")
  reset_data("portee")
  return (moy_portee, moy_etnt)

# Dessine les donnees en utilisant matplotlib
def draw_data(nb_generation, objectif):
  plt.subplot(221)
  plt.plot(get_data("moy_score"), "r-", get_data("max_score"), "b-", get_data("min_score"), "y-")
  plt.title("scores")
  plt.subplot(222)
  plt.plot(get_data("variance_score"), "-", color="black")
  plt.title("variance")
  plt.subplot(223)
  plt.plot(get_data("moy_portee"), "r-")
  plt.title("portee")
  plt.subplot(224)
  plt.plot(get_data("moy_etnt"), "-", color="black")
  plt.title("etnt")

