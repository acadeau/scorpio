# Hauteur de la coupe par defaut
HAUTEUR_COUPE = 50

# % de chance que la hauteur de coupe change
TAUX_VARIATION_HAUTEUR_COUPE = 70

# Taux de mutation des genes, compris entre 0.1 et 10
TAUX_MUTATION = 2

NB_POPULATION = 500
NB_GENERATION = 200
OBJECTIF = 350
