import random
import metier
from const import (TAUX_MUTATION)

# Retourne une nouvelle fonction permetant de generer les gênes.
# Elle permet d'initialiser les parametres "materiau_masse_volumique", "materiaux_young", "materiau_masse_volumique"
# C'est plus simple que de passer ces parametres a chaques appel de fonction (et ça permet notamment de l'utiliser dans une fonction map)
def create_generate_gene(materiaux_poisson, materiaux_young, materiau_masse_volumique) :
  def generate_gene(name):
      gene_randomizer = {
        "angle" : random.randint(0,90),
        "longueur_bras" : random.randint(1,50),
        "base_bras" : random.uniform(0.10,5),
        "hauteur_bras" : random.uniform(0.10,5),
        "diametre_fleche" : random.uniform(0.10,3),
        "longueur_corde" : random.randint(1,80),
        "longueur_fleche" : random.randint(1,80),
        "poisson" : random.choice(materiaux_poisson),
        "young" : random.choice(materiaux_young),
        "masse_volumique_fleche" : random.choice(materiau_masse_volumique)
      }
      return gene_randomizer[name]
  return generate_gene

# On initialise la fonction generate_gene avec les tableaux de Materiaux
generate_gene = create_generate_gene(metier.MATERIAUX_POISSON, metier.MATERIAUX_YOUNG, metier.MATERIAUX_MASSE_VOLUMIQUE)

# Recupere la valeur dans l'individu. Il convertit cette valeur en string ou en float 
# selon son type
def get_gene(name, individu):
  gene = next((gene for gene in individu.split(";") if name in gene), "")
  value_gene = gene.split("=")[-1]
  try:
    value_gene = float(value_gene)
  except ValueError:
    value_gene = int(value_gene)
  return value_gene

# Retourne un nouvel individu avec des valeurs aleatoire.
# Un individu est stocke dans une chaine de caractere
def create_individu(i):
  return "angle={};longueur_bras={};base_bras={};hauteur_bras={};diametre_fleche={};longueur_corde={};longueur_fleche={};poisson={};young={};masse_volumique_fleche={}".format(
    generate_gene("angle")
  , generate_gene('longueur_bras')
  , generate_gene('base_bras')
  , generate_gene('hauteur_bras')
  , generate_gene('diametre_fleche')
  , generate_gene('longueur_corde')
  , generate_gene('longueur_fleche')
  , generate_gene('poisson')
  , generate_gene('young')
  , generate_gene('masse_volumique_fleche'))


# Retourne un nouvel individu avec une nouvelle valeur pour 
# le gene passe en parametre
def update_individu(individu, name, value):
  return individu.replace("{}={}".format(name, get_gene(name, individu)), "{}={}".format(name, value))

# Mute un gene selectionne aleatoirement
# L'individu mutera que si une valeur aleatoire est inferieur aux TAUX_MUTATION
# definit dans const.py
def mutate(individu) :
  if random.uniform(0,100) < TAUX_MUTATION:
    all_genes = individu.split(";")
    mutation_key = all_genes[random.randint(0, len(all_genes) - 1)].split("=")[0]
    individu = update_individu(individu, mutation_key, generate_gene(mutation_key))
    
  return individu
    