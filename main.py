import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import mpld3
import individu
import data_manager 
import population as pop_func
from const import (NB_POPULATION, NB_GENERATION, OBJECTIF)

# Creer une nouvelle liste en fonction de la fonction passe en parametre
# Cette fonction permet de faire comme la fonction map tout en reduisant d'un
# niveau la profondeur.
# Ex : flat_map(lambda x: x*2, [[1,2],[2,3]]) --> [2,4,4,6]
def flat_map(func, list_to_change):
  new_list = list(map(func, list_to_change))
  return [item for sublist in new_list for item in sublist]

population = pop_func.generate_pop(NB_POPULATION)

# Deroulement de l'algorithme genetique
for i in range(NB_GENERATION):
  individu_scores = list(map(pop_func.fitness, population))
  # Permet juste de faire les courbes
  data_manager.register_score(individu_scores)
  # Permet juste de faire les courbes
  data_manager.calculate_moy_genes()
  couples = pop_func.selection(individu_scores, NB_POPULATION)
  population = list(map(individu.mutate, flat_map(pop_func.cross, couples)))
  print(i)

# Dessine les graphiques
data_manager.draw_data(NB_GENERATION, OBJECTIF)

# Lance un serveur pour voir les graphiques (contournement du probleme de GUI sous docker)
mpld3.show(ip="0.0.0.0")